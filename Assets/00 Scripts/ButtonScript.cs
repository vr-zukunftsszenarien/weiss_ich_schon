﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Cradle;
using Cradle.StoryFormats.Harlowe;
using static TwineTextPlayer;

public class ButtonScript : MonoBehaviour
{
    public void TestingClass()
    {

        string TestText = "============================================================================================================================" +
            Environment.NewLine +
            "========================================================            Debugging!!!!            ========================================================";

        Debug.Log(TestText);
    }

    public void keyPressed()
    {
        if (Input.GetKeyDown(KeyCode.F))
            Debug.LogError("Key F Pressed");
    }
}
