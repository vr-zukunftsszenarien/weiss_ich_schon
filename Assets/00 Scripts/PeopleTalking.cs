﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleTalking : MonoBehaviour
{
    public Animator tom;
    public Animator chris;
    public float speakingTime = 2f;
    float currentTime = 0f;
    bool who = false;


    // Update is called once per frame
    void Update()
    {
        if (!who && Time.time > currentTime)
        {
            tom.SetBool("tomTalking", true);
            chris.SetBool("chrisTalking", false);
            currentTime = (Time.time + speakingTime);            
            who = true;
        }
        if (who && Time.time > currentTime)
        {
            tom.SetBool("tomTalking", false);
            chris.SetBool("chrisTalking", true);
            currentTime = (Time.time + speakingTime);
            who = false;
        }
        


    }
}
