﻿using UnityEngine;
using UnityEngine.SceneManagement;
namespace Yarn.Unity.Example

{
    //[RequireComponent(typeof(Animator))]
    // Attach SpriteSwitcher to game object
    public class YarnSceneSwitcher : MonoBehaviour
    {
        public PMSceneManager pMSceneManager;


        [System.Serializable]
        public struct SpriteInfo
        {
            public string name;
            public Sprite sprite;
        }

        /// Create a command to use on a sprite
        [YarnCommand("nextScene")]
        public void GotoMainScene()
        {
            pMSceneManager.WeAreTransitioning = true;
            
        }
        [YarnCommand("scene0")]
        public void scene0()
        {
            pMSceneManager.sceneIndex = 0;
            pMSceneManager.WeAreTransitioningtoScene = true;
            
        }

        [YarnCommand("scene1")]
        public void scene1()
        {
            pMSceneManager.sceneIndex = 1;
            pMSceneManager.WeAreTransitioningtoScene = true;
            
        }
        [YarnCommand("scene2")]
        public void scene2()
        {
            pMSceneManager.sceneIndex = 2;
            pMSceneManager.WeAreTransitioningtoScene = true;
            
        }
        [YarnCommand("scene3")]
        public void scene3()
        {
            pMSceneManager.sceneIndex = 4;
            pMSceneManager.WeAreTransitioningtoScene = true;
        }
        [YarnCommand("scene4")]
        public void scene4()
        {
            pMSceneManager.sceneIndex = 6;
            pMSceneManager.WeAreTransitioningtoScene = true;
        }
        [YarnCommand("leave")]
        public void leave()
        {
            pMSceneManager.sceneIndex = 6;
            pMSceneManager.leaveGame = true;
        }
    }
}