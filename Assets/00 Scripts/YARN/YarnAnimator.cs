﻿using UnityEngine;
using System.Collections;

namespace Yarn.Unity.Example
{


    public class YarnAnimator : MonoBehaviour
    { 
        public Animator animator;

        /// Create a command to use on a sprite
        [YarnCommand("isaTalking")]
        public void IsaTalking()
        {
            animator.SetBool("isTalking", true);

        }

        [YarnCommand("isaNotTalking")]
        public void IsaNotTalking()
        {
            animator.SetBool("isTalking", false);

        }

        [YarnCommand("isaIsOnThePhone")]
        public void isaIsOnThePhone()
        {
            animator.SetBool("isOnThePhone", true);

        }

        [YarnCommand("isaIsNotOnThePhone")]
        public void isaIsNotOnThePhone()
        {
            animator.SetBool("isOnThePhone", false);

        }


    }

}