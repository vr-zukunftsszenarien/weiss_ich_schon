﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Cradle;

namespace Yarn.Unity.Example
{   
    public class YarnScripting : MonoBehaviour
    {
        Color color;
        public float rFloat;
        public float gFloat;
        public float bFloat;
        public float aFloat;
        public Image image;

        
        [YarnCommand("ColorINTRO")]
        public void Color0()
        {
            aFloat = 0.2235294f;
            rFloat = 0.1915717f;
            gFloat = 0.990566f;
            bFloat = 0.891757f;
            color = new Color(rFloat, gFloat, bFloat, aFloat);
            image.color = color;
            
        }
        [YarnCommand("ColorME")]
        public void Color1()
        {
            aFloat = 0.1764706f;
            rFloat = 1f;
            gFloat = 0.6072124f;
            bFloat = 0.03529412f;
            color = new Color(rFloat, gFloat, bFloat, aFloat);
            image.color = color;
        }
        [YarnCommand("ColorISA")]
        public void Color2()
        {
            aFloat = 0.15f;
            rFloat = 1f;
            gFloat = 1f;
            bFloat = 1f;
            color = new Color(rFloat, gFloat, bFloat, aFloat);
            image.color = color;
        }
        [YarnCommand("ColorALFRED")]
        public void Color3()
        {
            aFloat = 0.2784314f;
            rFloat = 0.2896938f;
            gFloat = 0.5849056f;
            bFloat = 0.3717637f;
            color = new Color(rFloat, gFloat, bFloat, aFloat);
            image.color = color;
        }
        [YarnCommand("ColorROBO")]
        public void ColorROBO()
        {
            aFloat = 0.1764706f;
            rFloat = 0.1529412f;
            gFloat = 0.1529412f;
            bFloat = 0.1529412f;
            color = new Color(rFloat, gFloat, bFloat, aFloat);
            image.color = color;
        }
    }
}