﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PMSceneManager : MonoBehaviour
{

    public Animator transition;
    public float transitionTime = 1f;
    [HideInInspector]
    public bool WeAreTransitioning = false;
    [HideInInspector]
    public bool WeAreTransitioningtoScene = false;
    [HideInInspector]    
    public bool leaveGame = false;
    [HideInInspector]
    public int sceneIndex = 0;

    void Update()
    {
        if (WeAreTransitioning)
        {
            
            LoadNextScene();
        }
        if (WeAreTransitioningtoScene)
        {
            
            LoadNextSceneTo(sceneIndex);
        }
        if (leaveGame)
        {
            QuitGame();
        }
    }

    public void LoadNextScene()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
        WeAreTransitioning = false;
    }
    public void LoadNextSceneTo(int index)
    {
        StartCoroutine(LoadLevel(index));

        WeAreTransitioningtoScene = false;
    }

    IEnumerator LoadLevel(int levelIndex)
    {
        // Play animation
        transition.SetTrigger("Crossfade_Start");

        //Wait
        yield return new WaitForSeconds(transitionTime);

        //Load Scene
        SceneManager.LoadScene(levelIndex);

    }
    public void QuitGame()
    {
        Debug.LogWarning("QUIT!");
        Application.Quit();
        leaveGame = false;

    }

}
