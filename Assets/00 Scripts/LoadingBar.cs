﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Yarn.Unity.Example
{


    public class LoadingBar : MonoBehaviour
    {
        public GameObject loadingScreen;
        public Slider slider;
        public Animator animator;

        /// Create a command to use on a sprite
        [YarnCommand("RoboLoading")]
        public void RoboLoading()
        {
            loadingScreen.SetActive(true);
            animator.SetTrigger("Loading");
        }






    }

}