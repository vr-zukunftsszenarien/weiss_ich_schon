﻿using UnityEngine;
using System.Collections;


namespace Yarn.Unity.Example
{


    public class YarnAlfred : MonoBehaviour
    {
        public Animator animator;
        public Animator moveYarn;

        //Vector3 positionAlfred;// = (1, 1, 1);
        /// Create a command to use on a sprite
        [YarnCommand("AlfredTalking")]
        public void AlfredTalking()
        {
            animator.SetBool("isWalking", false);
            animator.SetBool("isTalking", true);

        }

        [YarnCommand("AlfredNotTalking")]
        public void AlfredNotTalking()
        {
            animator.SetBool("isTalking", false);

        }

        [YarnCommand("AlfredWalking")]
        public void AlfredWalking()
        {
            animator.SetBool("isWalking", true);

        }

        [YarnCommand("AlfredWalkingBack")]
        public void AlfredWalkingBack()
        {
            animator.SetBool("isWalkingBack", true);

        }

        [YarnCommand("SwitchToAlfred")]
        public void SwitchToAlfred()
        {
            moveYarn.SetTrigger("MoveYarn");

        }


    }

}